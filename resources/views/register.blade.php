@extends('layouts.master')

@section('header_title')
Register page
@endsection

@section('title')
Sign Up Form
@endsection

@section('content')
<form action="/welcome" method="POST">
  @csrf

  <label for="fname">First name:</label><br>
  <input type="text" name="fname" value="" required ><br>
  <label for="lname">Last name:</label><br>
  <input type="text" name="lname" value="" required ><br>

  <p>Gender : </p>
  <input type="radio" name="gender" value="Man" required >
  <label for="gender">Man</label><br>
  <input type="radio" name="gender" value="Woman" required >
  <label for="gender">Woman</label><br>
  <input type="radio" name="gender" value="Other" required >
  <label for="gender">Other</label><br><br>

  <label for="nationality">Nationality:</label>
  <select name="nationality" >
      <option value="indonesia" selected required >Indonesia</option>
      <option value="malaysia">Malaysia</option>
      <option value="singapore">Singapore</option>
      <option value="papua nugini">Papua Nugini</option>
  </select> <br><br>

  <input type="checkbox" name="lang1" value="Bahasa Indonesia">
  <label for="lang1"> Bahasa Indonesia</label><br>
  <input type="checkbox" name="lang2" value="English">
  <label for="lang2"> English</label><br>
  <input type="checkbox" name="lang3" value="Arabic">
  <label for="lang2"> Arabic</label><br>
  <input type="checkbox"name="lang4" value="Japanese">
  <label for="lang4"> Japanese</label><br><br>

  <label for="bio">Bio :</label><br>

  <textarea name="bio" rows="4" cols="50" required></textarea><br>

  <input type="submit" value="SignUp">

</form> 
@endsection