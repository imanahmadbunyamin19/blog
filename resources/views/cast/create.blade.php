@extends('layouts.master')

@section('header_title')
Tambah Data 
@endsection

@section('title')
Tambah Pemeran
@endsection

@section('content')
<div>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nama </label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="age">Umur</label>
                <input type="number" class="form-control" name="age" id="age" placeholder="">
                @error('age')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea id="bio" name="bio" class="form-control" rows="4" cols="50"  placeholder="Masukan Bio"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
</div>
@endsection
