@extends('layouts.master')

@section('header_title')
Review Film
@endsection

@section('title')
Data Pemeran
@endsection

@section('content')

<h2>Menampilkan Data Pemeran {{$cast->id}}</h2> <br>

<h4>Nama    :{{$cast->nama}}</h4>
<h4>Umur    :{{$cast->umur}}</h4>
<h4>Bio     :{{$cast->bio}}</h4>

@endsection