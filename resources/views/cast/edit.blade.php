@extends('layouts.master')

@section('header_title')
Review Film
@endsection

@section('title')
Edit Data Pemeran
@endsection

@section('content')
<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nama Pemeran</label>
            <input type="text" class="form-control" name="name" value="{{$cast->nama}}" id="name" placeholder="">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="age">Umur</label>
            <input type="number" class="form-control" name="age"  value="{{$cast->umur}}"  id="age" placeholder="">
            @error('age')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea id="bio" name="bio" class="form-control" rows="4" cols="50"   id="bio" placeholder="">{{$cast->bio}} </textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection