@extends('layouts.master')

@section('header_title')
Review Film
@endsection

@section('title')
Data Pemeran
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary my-1">Tambah Pemeran</a>
        <table class="table">
            <thead class="table table-striped table-dark ">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td display: inline>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Tidak ada data ditemukan</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection