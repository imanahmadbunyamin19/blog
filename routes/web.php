<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//route index / home
Route::get('/', 'HomeController@index');

//Route ke halaman register
Route::get('/register', 'AuthController@register');

//Route ke halaman welcome
Route::post('/welcome', 'AuthController@welcome');

//Route ke halaman table
Route::get('/table', function(){
    return view('layouts.pages.table');
});

//Route ke halaman data-tables
Route::get('/data-tables', function(){
    return view('layouts.pages.data-tables');
});


//TUGAS CRUD CAST
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');